# Techie para mostrar el uso del patrón builder #

En este proyecto vamos a ver como utilizar el patrón builder por medio de un ejercicio.

### ¿Que es el patrón builder? ###

El patrón *Builder* es un patrón creacional cuyo objetivo es **instanciar objetos complejos** que generalmente están **compuestos por varios elementos** y que admiten **diversas configuraciones**.

### ¿Para que sirve? ###

* Permite la creación de un objeto complejo, a partir de una variedad de partes que contribuyen individualmente a la creación y ensamblación del objeto mencionado.

* Centraliza el proceso de creación en un único punto, de tal forma que el mismo proceso de construcción pueda crear representaciones diferentes.

### Diagrama de clases ###

![builder-diagram.png](https://bitbucket.org/repo/6L4dn9/images/1719583805-builder-diagram.png)

* Producto
* * El objeto complejo bajo construcción
* Builder
* * Interfaz abstracta para crear producto
* Concrete builder
* * Implementación del builder
* * Construye y reúne las partes necesarias para construir los productos
* Director
* * Construye un objeto usando el patrón builder

#### ¿Como se usa? ####

1. El Cliente crea el objeto Director y lo configura con el objeto Builder deseado.
2. El Director notifica al constructor cuando una parte del Producto se debe construir.
3. El Builder maneja los requerimientos desde el Director y agrega partes al producto.
4. El Cliente recupera el Producto desde el constructor.


### Ejercicio:###

Utilizando el patrón builder hacer pasar los tests existentes. 

**Aclaración 1:** Solo se puede modificar las clases dentro del paquete (com.somospnt.techiebuilder.builder) y los tests.

**Aclaración 2:** Las clases PizzaBuilder, Cocina y FugazzetaPizzaBuilder no se deben modificar. 

**Aclaración 3:** No se debe crear ninguna clase extra.

**Hay un pdf dentro de la carpeta doc del proyecto que contiene una breve explicación del patrón, sus ventajas, la consigna del ejercicio y su respuesta.**

### Desarrollador:###

Manuel Sanchez Avalos