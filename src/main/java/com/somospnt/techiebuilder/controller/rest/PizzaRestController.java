package com.somospnt.techiebuilder.controller.rest;

import com.somospnt.techiebuilder.domain.Pizza;
import com.somospnt.techiebuilder.service.PizzaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/pizza")
public class PizzaRestController {

    @Autowired
    private PizzaService pizzaService;

    @PostMapping
    public Pizza guardar(@RequestBody Pizza pizza) {
        return pizzaService.guardar(pizza);
    }
}
