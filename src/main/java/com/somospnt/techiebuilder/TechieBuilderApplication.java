package com.somospnt.techiebuilder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechieBuilderApplication {

    public static void main(String[] args) {
        SpringApplication.run(TechieBuilderApplication.class, args);
    }
}
