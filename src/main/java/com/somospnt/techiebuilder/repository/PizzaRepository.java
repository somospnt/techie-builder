package com.somospnt.techiebuilder.repository;

import com.somospnt.techiebuilder.domain.Pizza;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PizzaRepository extends JpaRepository<Pizza, Long> {

}
