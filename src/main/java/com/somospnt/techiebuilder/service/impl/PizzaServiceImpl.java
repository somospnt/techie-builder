package com.somospnt.techiebuilder.service.impl;

import com.somospnt.techiebuilder.domain.Pizza;
import com.somospnt.techiebuilder.domain.TipoPizza;
import com.somospnt.techiebuilder.repository.PizzaRepository;
import com.somospnt.techiebuilder.service.PizzaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PizzaServiceImpl implements PizzaService {

    @Value("${com.somospnt.techiebuilder.pizza.margarita}")
    private Double precioMargarita;
    @Value("${com.somospnt.techiebuilder.pizza.fugazzeta}")
    private Double precioFugazzeta;
    @Value("${com.somospnt.techiebuilder.pizza.napolitana}")
    private Double precioNapolitana;

    @Autowired
    PizzaRepository pizzaRepository;

    @Override
    public Pizza guardar(Pizza pizza) {
        return pizzaRepository.save(actualizarPrecio(pizza));
    }

    private Pizza actualizarPrecio(Pizza pizza) {
        if (pizza.getTipoPizza().equals(TipoPizza.MARGARITA)) {
            pizza.setPrecio(precioMargarita);
        } else if (pizza.getTipoPizza().equals(TipoPizza.FUGAZZETA)) {
            pizza.setPrecio(precioFugazzeta);
        } else if (pizza.getTipoPizza().equals(TipoPizza.NAPOLITANA)) {
            pizza.setPrecio(precioNapolitana);
        }
        return pizza;
    }

}
