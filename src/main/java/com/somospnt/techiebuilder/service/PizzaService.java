package com.somospnt.techiebuilder.service;

import com.somospnt.techiebuilder.domain.Pizza;

public interface PizzaService {

    Pizza guardar(Pizza pizza);
}
