package com.somospnt.techiebuilder.domain;

public enum TipoPizza {

    MARGARITA("Pizza margarita"),
    FUGAZZETA("Pizza de fugazzeta"),
    NAPOLITANA("Pizza napolitana");

    TipoPizza(String descripcion) {
        this.descripcion = descripcion;
    }

    private final String descripcion;

    public String getDescripcion() {
        return this.descripcion;
    }

}
