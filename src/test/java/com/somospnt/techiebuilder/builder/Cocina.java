package com.somospnt.techiebuilder.builder;

import com.somospnt.techiebuilder.domain.Pizza;

/**
 * Director*
 */
public class Cocina {

    private PizzaBuilder pizzaBuilder;

    public void setPizzaBuilder(PizzaBuilder pb) {
        pizzaBuilder = pb;
    }

    public Pizza getPizza() {
        return pizzaBuilder.getPizza();
    }

    public void construirPizza() {
        pizzaBuilder.crearNuevaPizza();
        pizzaBuilder.buildMasa();
        pizzaBuilder.buildPrecio();
        pizzaBuilder.buildTipoPizza();
        pizzaBuilder.buildPorciones();
    }
}
