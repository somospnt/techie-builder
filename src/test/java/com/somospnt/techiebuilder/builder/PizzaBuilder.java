package com.somospnt.techiebuilder.builder;

import com.somospnt.techiebuilder.domain.Pizza;

/**
 * Abstract Builder*
 */
public abstract class PizzaBuilder {

    protected Pizza pizza;

    public Pizza getPizza() {
        return pizza;
    }

    public void crearNuevaPizza() {
        pizza = new Pizza();
    }

    public abstract void buildMasa();

    public abstract void buildPrecio();

    public abstract void buildTipoPizza();

    public abstract void buildPorciones();
}
