package com.somospnt.techiebuilder.builder;

import com.somospnt.techiebuilder.domain.TipoPizza;

/**
 * ConcreteBuilder*
 */
public class FugazzetaPizzaBuilder extends PizzaBuilder {

    @Override
    public void buildMasa() {
        pizza.setMasa("suave");
    }

    @Override
    public void buildPrecio() {
        pizza.setPrecio(new Double(200));
    }

    @Override
    public void buildTipoPizza() {
        pizza.setTipoPizza(TipoPizza.FUGAZZETA);
    }

    @Override
    public void buildPorciones() {
        pizza.setPorciones(8);
    }
}
