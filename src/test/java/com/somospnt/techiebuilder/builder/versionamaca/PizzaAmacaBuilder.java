package com.somospnt.techiebuilder.builder.versionamaca;

import com.somospnt.techiebuilder.domain.Pizza;
import com.somospnt.techiebuilder.domain.TipoPizza;

public class PizzaAmacaBuilder {

    private Pizza pizza;

    public static PizzaAmacaBuilder tipica() {
        PizzaAmacaBuilder builder = new PizzaAmacaBuilder();
        Pizza pizza = new Pizza();
        pizza.setMasa("suave");
        pizza.setPorciones(8);
        pizza.setTipoPizza(TipoPizza.MARGARITA);
        pizza.setPrecio(new Double(100));
        builder.pizza = pizza;
        return builder;
    }

    public PizzaAmacaBuilder conPorciones(int porciones) {
        pizza.setPorciones(porciones);
        return this;
    }

    public PizzaAmacaBuilder conMasa(String masa) {
        pizza.setMasa(masa);
        return this;
    }

    public PizzaAmacaBuilder conTipoPizza(TipoPizza tipoPizza) {
        pizza.setTipoPizza(tipoPizza);
        return this;
    }

    public PizzaAmacaBuilder conPrecio(Double precio) {
        pizza.setPrecio(precio);
        return this;
    }

    public Pizza build() {
        return this.pizza;
    }
}
