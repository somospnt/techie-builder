package com.somospnt.techiebuilder.service;

import com.somospnt.techiebuilder.TechieBuilderApplication;
import com.somospnt.techiebuilder.builder.Cocina;
import com.somospnt.techiebuilder.builder.FugazzetaPizzaBuilder;
import com.somospnt.techiebuilder.builder.PizzaBuilder;
import com.somospnt.techiebuilder.builder.versionamaca.PizzaAmacaBuilder;
import com.somospnt.techiebuilder.domain.Pizza;
import com.somospnt.techiebuilder.domain.TipoPizza;
import javax.transaction.Transactional;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TechieBuilderApplication.class)
@Transactional
public class PizzaServiceTest {

    @Autowired
    PizzaService pizzaService;

    /**
     * Sin Builder *
     */
    @Test
    public void guardar_pizzaFugazzeta_guardaPizzaFugazzetaConPrecioActualizado() {
        Pizza pizza = new Pizza();
        pizza.setId(1L);
        pizza.setMasa("suave");
        pizza.setTipoPizza(TipoPizza.FUGAZZETA);
        pizza.setPrecio(new Double(200));
        pizza.setPorciones(8);

        Pizza pizzaGuardada = pizzaService.guardar(pizza);
        assertNotNull(pizzaGuardada);
        assertEquals(TipoPizza.FUGAZZETA, pizzaGuardada.getTipoPizza());
        assertEquals(new Double(75), pizzaGuardada.getPrecio());
    }

    /**
     * Con Builder: Hay que crear una Pizza Fugazzeta y guardarla. Consejo:
     * Seguir los pasos de uso del PDF.
     */
    @Test
    public void guardar_PizzaFugazzeta_guardaPizzaFugazzetaConPrecioActualizado() {

        Pizza pizza = new Pizza();

        //No se debe modificar lo siguiente:
        Pizza pizzaGuardada = pizzaService.guardar(pizza);
        assertNotNull(pizzaGuardada);
        assertEquals(TipoPizza.FUGAZZETA, pizzaGuardada.getTipoPizza());
        assertEquals(new Double(75), pizzaGuardada.getPrecio());
    }

    /**
     * Con Builder: Hay que crear una Pizza Napolitana y guardarla. Consejo:
     * Seguir los pasos de uso del PDF.
     */
    @Test
    public void guardar_PizzaNapolitana_guardaPizzaNapolitanaConPrecioActualizado() {

        Pizza pizza = new Pizza();

        //No se debe modificar lo siguiente:
        Pizza pizzaGuardada = pizzaService.guardar(pizza);
        assertNotNull(pizzaGuardada);
        assertEquals(TipoPizza.NAPOLITANA, pizzaGuardada.getTipoPizza());
        assertEquals(new Double(100), pizzaGuardada.getPrecio());
    }

    /**
     * Con Builder: Hay que crear una Pizza Margarita y guardarla. Consejo:
     * Seguir los pasos de uso del PDF.
     */
    @Test
    public void guardar_PizzaMargarita_guardaPizzaMargaritaConPrecioActualizado() {

        Pizza pizza = new Pizza();

        //No se debe modificar lo siguiente:
        Pizza pizzaGuardada = pizzaService.guardar(pizza);
        assertNotNull(pizzaGuardada);
        assertEquals(TipoPizza.MARGARITA, pizzaGuardada.getTipoPizza());
        assertEquals(new Double(50), pizzaGuardada.getPrecio());
    }

    /**
     * Con Builder Version Amaca: No hay que hacer nada, solo es para mostrar la
     * manera en la que lo usamos nosotros. ¿Utiliza el patrón builder? Para
     * debatir...
     */
    @Test
    public void guardar_PizzaMargaritaCon4PorcionesYConMasaGruesa_guardaPizzaMargaritaConPrecioActualizado() {
        Pizza pizza = PizzaAmacaBuilder.tipica().conMasa("Gruesa").conPorciones(4).build();
        Pizza pizzaGuardada = pizzaService.guardar(pizza);
        assertNotNull(pizzaGuardada);
        assertEquals(TipoPizza.MARGARITA, pizzaGuardada.getTipoPizza());
        assertEquals(new Double(50), pizzaGuardada.getPrecio());
        assertEquals("Gruesa", pizzaGuardada.getMasa());

    }
}
