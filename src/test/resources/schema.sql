DROP TABLE IF EXISTS pizza;

CREATE TABLE pizza (
    id              INTEGER IDENTITY PRIMARY KEY,
    masa            VARCHAR(20) NOT NULL,
    tipo_pizza      VARCHAR(20) NOT NULL,
    precio          INTEGER NOT NULL,
    porciones       INTEGER NOT NULL
);
